from flask import url_for, json


def test_connections(client, neo_db):
    resp = client.get(url_for("connections_2d", user_id=1))
    assert resp.status_code == 200
    data = json.loads(resp.data)
    assert "users" in data
    assert set(data["users"])  == {5, 6, 7}


def test_limit(client, neo_db):
    resp = client.get(url_for("connections_2d", user_id=1) + "?limit=2")
    data = json.loads(resp.data)
    assert set(data["users"])  == {5, 6}
    resp = client.get(url_for("connections_2d", user_id=1) + "?limit=2&page=1")
    data = json.loads(resp.data)
    assert set(data["users"])  == {7}


def test_user_not_found(client, neo_db):
    resp = client.get(url_for("connections_2d", user_id=10000000))
    assert resp.status_code == 404


def test_is_connected(client, neo_db):
    def check(user, second, result):
        resp = client.get(url_for("is_connected", user_id=user, second_id=second))
        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert data["connected"] == result
    check(1, 7, True)
    check(1, 2, False)
    check(1, 3, False)
    check(1, 8, False)
    check(3, 8, True)
