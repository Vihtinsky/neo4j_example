import pytest
from .app_config import TestingConfig
from app import app
from neodb import get_driver


@pytest.fixture(scope="session")
def flask_app():
    app.config.from_object(TestingConfig)
    app.test_request_context().push()
    return app


@pytest.fixture
def client(flask_app):
    return app.test_client()


@pytest.fixture(scope="session")
def neo_db(flask_app):
    session = get_driver().session()
    # Create 8 users
    for i in range(1, 9):
        session.run("MERGE (n:User {name: {name}, id: {id}})",
                        {"name": "User{}".format(i), "id": i})
    # Create connections
    connections = {1: [3, 4],
                   2: [7],
                   3: [1, 4, 5],
                   4: [1, 3, 6],
                   5: [3, 6, 7],
                   6: [4, 5, 7],
                   7: [5, 6, 8],
                   8: [7]}
    for user_id, user_cons in connections.items():
        for id_con in user_cons:
            session.run("MATCH (u:User { id: {id} }), (u1:User { id: {id_con} }) "
                        "MERGE (u)-[:CONNECTS]-(u1)",
                        {"id": user_id, "id_con": id_con})
    yield
    # After tests finished
    if TestingConfig.CLEAR_DB:
        session.run("MATCH (u: User)-[rel]-(c:User) DELETE u,c,rel")
