from config import Config
from neo4j.v1 import GraphDatabase, basic_auth


class TestingConfig(Config):
    DEBUG = True
    CLEAR_DB = False
    NEO4J = GraphDatabase.driver("FILL ME", auth=basic_auth("FILL", "AUTH"))
