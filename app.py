from flask import Flask, jsonify, current_app, request
from config import Config
from neodb import get_driver

app = Flask(__name__)
app.config.from_object(Config)


def _user_exist(user_id, session):
    user = session.run("MATCH (a:User) WHERE a.id={id} RETURN a.id ", {"id": user_id})
    return bool(user.peek())


@app.route("/connections_2d_3d/<int:user_id>")
def connections_2d(user_id):
    """ Takes optional query params 'limit' and 'page'
    default limit = config["LIMIT"], page = 0 """

    limit = int(request.values.get("limit", current_app.config["LIMIT"]))
    page = int(request.values.get("page", 0))
    skip = page * limit
    with get_driver().session() as session:
        if not _user_exist(user_id, session):
            return jsonify({"error": "User not found"}), 404
        connections = session.run(
            "MATCH (a:User {id: {id}} )"
            "-[:CONNECTS*2..3]-(u:User)"
            "WHERE NOT ((a:User)-[:CONNECTS]-(u:User)) AND u.id <> a.id "
            "RETURN DISTINCT u.id as id "
            "SKIP {skip} LIMIT {limit}",
            {"id": user_id, "limit": limit, "skip": skip}
        )
        ids = [i['id'] for i in connections]
    return jsonify({"users": ids}), 200


@app.route("/is_connected/<int:user_id>/<int:second_id>")
def is_connected(user_id, second_id):
    if user_id == second_id:
        return jsonify({"connected": False}), 200

    with get_driver().session() as session:
        if not _user_exist(user_id, session) or\
           not _user_exist(second_id, session):
            return jsonify({"error": "User not found"}), 404

        match = session.run(
            "MATCH (a:User {id: {id}} )-[:CONNECTS*2..3]-(u:User {id: {id2}} )"
            "WHERE NOT ((a:User)-[:CONNECTS]-(u:User))"
            "RETURN u.id",
            {"id": user_id, "id2": second_id})
        connected = bool(match.peek())

    return jsonify({"connected": connected}), 200


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, threaded=True)
