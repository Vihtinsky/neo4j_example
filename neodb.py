from flask import current_app


def get_driver():
    return current_app.config["NEO4J"]
