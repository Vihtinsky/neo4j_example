## Neo4j test project

# Requeriments

Python 3.5, Neo4j 2.0 +. Python module could be installed with

```
$ pip -r requirements.txt
```

You should create local_config.py module, example in local_config.py.def

# Runnig tests

There is additional requeriments for tests - tests/test_requirements.txt
Provide testing Neo4j connection params in tests/app_config.py.
Run tests with py.test
```
$ py.test tests
```

# Measure performace

- Populate neo4j with test data

```
$ python performance_measure/populate_users.py

$ python performance_measure/populate_connections.py
```

- Install additional python modules

```$ pip install -r performance_measure/requirements.txt```

- Start gunicorn server

```$ ./start_gunicorn.sh```

- Run measure script

```$ python performance_measure/measure.py```
