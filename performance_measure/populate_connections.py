import sys
import os
import csv
import random

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from config import Config

# SET path to neo import dir
NEO_IMPORT_DIR = "/home/ilia/projects/remote_com/neo4j-community-3.1.3/import"

driver = Config.NEO4J
session = driver.session()

print("Creating CSV for connections")
group_size = 10
n_groups = int(2000000 / group_size)
with open(os.path.join(NEO_IMPORT_DIR, "connections.csv"), "w") as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(["id1", "id2"])
    # Create connected groups of size 10
    for i in range(n_groups):
        for id1 in range(i * group_size + 1, i * group_size + group_size):
            for id2 in range(id1 + 1, i * group_size + group_size + 1):
                writer.writerow((id1, id2))

    # Connect groups randomly
    def rand_user():
        return random.randint(1, 10)
    for i in range(n_groups):
        # make 1 - 3 connections
        for _ in range(random.randint(1, 3)):
            other_group = random.randint(0, n_groups - 1)
            if other_group == i:
                continue
            writer.writerow((i * group_size + rand_user(), other_group * group_size + rand_user()))
print("Execute LOAD CSV")
session.run("USING PERIODIC COMMIT 10000 LOAD CSV with headers FROM 'file:///connections.csv' as row "
            "MATCH (u:User { id: toInteger(row.id1) }), (u1:User { id: toInteger(row.id2) }) "
            "MERGE (u)-[:CONNECTS]-(u1)")
session.close()
