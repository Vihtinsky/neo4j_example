import requests
import random
import time
from multiprocessing import Process, Queue
import pandas as pd
import json

NUM = 3
NUM_PROCESSES = 1000
TEST = "measure"
data = Queue()
errors = Queue()
TITLE = "Measure neo_api"
SHOW_HISTOGRAM = True


def measure():
    start = time.time()
    r = requests.get('http://localhost:8080/connections_2d_3d/' + str(random.randint(1, 2000000)))
    takes = time.time() - start
    if r.status_code != 200:
        return takes, "Wrong status"
    try:
        data = json.loads(r.text)
        print(".", end="")
        assert "users" in data
    except:
        return takes, "Not returning results"
    return takes, False


def n_requests(process_id):
    def func():
        for i in range(NUM):
            try:
                t, error = globals()[TEST]()
                if error:
                    errors.put(error)
                data.put((t, process_id))
            except Exception as e:
                errors.put(str(e))
            time.sleep(0.01)
    return func

processes = []
for i in range(NUM_PROCESSES):
    processes.append(Process(target=n_requests(i)))

overall = time.time()
for p in processes:
    p.start()
for p in processes:
    p.join()
times = []

while True:
    try:
        tm, process_id = data.get(False)
        times.append(tm)
    except:
        break
series = pd.Series(times)
err = []
if not errors.empty():
    while True:
        try:
            err.append(errors.get(False))
        except:
            break
    print("Errors:", len(err))

print("\n")
overall_time = time.time() - overall
print("Min time sec:", series.min())
print("Max time sec:", series.max())
print("Avg time sec:", series.mean())
print("Takes {} for makes {} request with {} processes".format(overall_time, NUM * NUM_PROCESSES, NUM_PROCESSES))

if SHOW_HISTOGRAM:
    hi = series.hist(bins=30)
    title = "{} \n {} requests, {} simultaneous ({} sec)".format(TITLE, NUM * NUM_PROCESSES, NUM_PROCESSES, overall_time)
    if err:
        title += "\n {} errors".format(len(err))
    hi.set_title(title)
    hi.set_xlabel("time sec")
    hi.set_ylabel("Number of requests")
    hi.figure.show()
    input()
