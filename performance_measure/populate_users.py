import sys
import os
import csv
import random

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from config import Config

# SET path to neo import dir
NEO_IMPORT_DIR = "/home/ilia/projects/remote_com/neo4j-community-3.1.3/import"

driver = Config.NEO4J
session = driver.session()

# Create 2 Millions users

# WRITE to csv first
print("Creating CSV for users")
with open(os.path.join(NEO_IMPORT_DIR, "users.csv"), "w") as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(["name", "id"])
    for i in range(1, 2000001):
        writer.writerow(("User{}".format(i), i))
print("Execute LOAD CSV")
session.run("USING PERIODIC COMMIT 10000 LOAD CSV with headers FROM 'file:///users.csv' as row CREATE (:User {id: toInteger(row.id), name: row.name})")
print("Creating index on :User(id)")
session.run("create index on :User(id)")
session.close()
